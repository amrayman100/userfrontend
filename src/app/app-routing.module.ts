
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserlistComponent } from './userlist/userlist.component';
import {LoginComponent} from './login/login.component';
import { AuthGuard } from './_helpers/auth.guard';
import { RoleGuard} from './_helpers/role.guard';
import { UserComponent } from './_components/user/user.component'
import {AdminComponent} from './_components/admin/admin.component';
//import {UserComponent} from './dashboard/user/user.component'
//import DashboardComponent from './dashboard/'

const routes: Routes = [
  { path: '', component: AdminComponent, canActivate: [AuthGuard , RoleGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'userview', component: UserComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})






export class AppRoutingModule { }