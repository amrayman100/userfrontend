import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import {User} from './_models/user';
import {Group} from './_models/group';
import { AuthenticationService } from './_services/authentication.service';
@Injectable({
  providedIn: 'root'
})
export class UsersService {

  currentUser:User;

  constructor(private http: HttpClient,private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
   }

  getUsers(){
  

    return this.http.get<Object[]>("http://localhost:8880/users/user/");
  }

  getUser(username:string){
    return this.http.get<Object[]>("/api/users/user/search/"+username);
  }

  async getCurrentUser(){

    return await this.currentUser;
  }




  async updateProfile(username:string,name:string,email:string,phonenumber:string){
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        
      })
    };
    if(this.currentUser.username!=username){
        
        await this.http.put('/api/users/user/username/'+this.currentUser.username,username,httpOptions).toPromise();
        this.currentUser.username = username;
        
    }

    if(this.currentUser.name!=name){

      await this.http.put('/api/users/user/name/'+this.currentUser.username,name,httpOptions).toPromise();
      this.currentUser.name = name;
    }

    if(this.currentUser.email!=email){
      await this.http.put('/api/users/user/email/'+this.currentUser.email,email,httpOptions).toPromise();
      this.currentUser.email = email;
    }

    if(this.currentUser.number!=phonenumber){
      await this.http.put('/api/users/user/phone/'+this.currentUser.username,phonenumber,httpOptions).toPromise();
      this.currentUser.number = phonenumber;
    }

    localStorage.setItem('currentUser', JSON.stringify(this.currentUser));

  }


  async updateUser(username:string,name:string,email:string,phonenumber:string,u:User){
    //let u:User;
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        
      })
    };
    if(this.currentUser.username!=username){
        
        await this.http.put('/api/users/user/username/'+username,username,httpOptions).toPromise();
        u.username = username;
        
    }

    if(this.currentUser.name!=name){
  
      await this.http.put('/api/users/user/name/'+username,name,httpOptions).toPromise();
      u.name = name;
    }

    if(this.currentUser.email!=email){
      await this.http.put('/api/users/user/email/'+username,email,httpOptions).toPromise();
      u.email = email;
    }

    if(this.currentUser.number!=phonenumber){
      await this.http.put('/api/users/user/phone/'+username,phonenumber,httpOptions).toPromise();
      u.phoneNum = phonenumber;
      u.number = phonenumber;
    }

    return u;

  }

  async deleteUser(u:User){
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        
      })
    };
  
    await this.http.delete('/api/users/user/delete/'+u.username,httpOptions).toPromise();
    u.del = true;
    return u;
  }

  async UndoDeleteUser(u:User){
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        
      })
    };
    
    await this.http.put('/api/users/user/undo/'+u.username,httpOptions).toPromise();
    u.del = true;
    return u;
  }

  async deleteUserFromGroup(u:User, g:Group){
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        
      })
    };

    await this.http.delete('/api/users/user/group/'+u.username+'/'+g.id,httpOptions).toPromise();

    return g;

  }



}
