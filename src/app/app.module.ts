import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule , HTTP_INTERCEPTORS} from '@angular/common/http';
import { UserlistComponent } from './userlist/userlist.component';
import {UserlistComponent2} from './_components/userlist/userlist.component'
import { LoginComponent } from './login/login.component';
import { BasicAuthInterceptor} from './_helpers/basic-auth.interceptor';
import {ErrorInterceptor }  from './_helpers/error.interceptor';
import { HeaderComponent } from './_components/header/header.component';
import { UserComponent } from './_components/user/user.component';
import { ProfileComponent } from './_components/profile/profile.component';
import { GrouplistComponent } from './_components/grouplist/grouplist.component';
import { EditprofileComponent } from './_components/editprofile/editprofile.component';
import { GroupComponent } from './_components/group/group.component';
import { ViewuserComponent } from './_components/viewuser/viewuser.component';
import { AdminComponent } from './_components/admin/admin.component';
import { EdituserComponent } from './_components/edituser/edituser.component';
import { EditgroupComponent } from './_components/editgroup/editgroup.component';
import { CreategroupComponent } from './_components/creategroup/creategroup.component';


@NgModule({
  declarations: [
    AppComponent,
    UserlistComponent,
    LoginComponent,
    HeaderComponent,
    UserComponent,
    ProfileComponent,
    UserlistComponent2,
    GrouplistComponent,
    EditprofileComponent,
    GroupComponent,
    ViewuserComponent,
    AdminComponent,
    EdituserComponent,
    EditgroupComponent,
    CreategroupComponent
  
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule   
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

],
  bootstrap: [AppComponent]
})
export class AppModule { }
