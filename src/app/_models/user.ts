import { Group } from './group';

export class User {
    id?: number;
    username?: string;
    password?: string;
    name?: string;
    email?: string;
    role?: string;
    number?: string;
    authdata?: string;
    groups?:Group[];
    phoneNum?:string;
    del?:boolean;
}