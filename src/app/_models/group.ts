import { User } from './user';

export class Group{
    id?:number;
    name?:string;
    desc?:string;
    users?:User[];

}