import { Injectable } from '@angular/core';
import {Group} from '../_models/group'
import {User} from '../_models/user'
import {UsersService} from '../users.service'
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { AuthenticationService } from '../_services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class GroupsService {

  currentUser : User;
  constructor(private http: HttpClient , private user_http:UsersService,private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
   }

  getGroups(){
    return this.http.get<Object[]>("/api/users/group/");
  }

  getGroupUsers(groupName:string){
    return this.http.get<Object[]>("/api/users/group/"+groupName+"/users");
  }

  getGroupUsers2(id:number){
    return this.http.get<Object[]>("/api/users/group/"+id+"/all");
  }

  deleteGroup(group:Group){
    return this.http.delete("/api/users/group/"+group.id);
  }

  async getGroupUsers3(id:number){
    let temp:User[]
    let users:User[]
     await this.http.get<User[]>("/api/users/user/get").toPromise().then(res=>{
       users = res
     });

     console.log(users)
     let ret :User[] = [];
     var i;
     console.log(users[0].groups);
     for(i = 0 ; i < users.length ; i++){
       var j;
        for(j = 0 ; j < users[i].groups.length;j++){
          if(id == users[i].groups[j].id){
            ret.push(users[i]);
            
          }
        }

     }

    return ret;

  }

  async getUnSubbed(u1:User[]){
    let users:User[]
    await this.http.get<User[]>("/api/users/user/get").toPromise().then(res=>{
      users = res
    });
   
    let ret :User[] = [];
    for(var i = 0 ; i < users.length ; i++){
      if(!u1.some(user => user.username == users[i].username)){
          ret.push(users[i]);
      }
    }
    
    return ret;

  }

  
  addUserToGroup(g:Group,u:User){
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        
      })
    };
 
    return this.http.post('/api/users/user/group/'+u.username+"/"+g.id,null,httpOptions);
  

  }




  async updateGroup(name:string,desc:string,g:Group){

    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        
      })
    };
    await this.http.put('/api/users/group/name/'+g.id+"/",name,httpOptions).toPromise();
    g.name = name;
    await this.http.put('/api/users/group/desc/'+g.id+"/",desc,httpOptions).toPromise();
    g.desc = desc;
    return g;

  }

  newGroup(name2:string,desc2:string){
    let Group = {
      name : name2,
      description: desc2,
    
    }

    return this.http.post("/api/users/group/" , Group);
  }


 
  
}
