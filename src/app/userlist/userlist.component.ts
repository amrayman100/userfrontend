import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service' 

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.scss']
})
export class UserlistComponent implements OnInit {
  users : Object;
  constructor(private _http :UsersService) { }

  ngOnInit() {
    this._http.getUsers().toPromise().then(data =>{
      this.users = data;
 
    });

  }

  async postUser(){

 


    await this._http.getUsers().toPromise().then(data =>{
    this.users = data;

    });


  }



}
