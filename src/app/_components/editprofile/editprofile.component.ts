import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators , ValidatorFn , AbstractControl ,AsyncValidatorFn , ValidationErrors} from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../_services/authentication.service';
import { UsersService } from '../../users.service' 
import {User} from '../../_models/user';
import { Observable } from "rxjs";
import {usernameAsyncValidator} from '../userAsyncValidation';


@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.scss']
})
export class EditprofileComponent implements OnInit {
    currentUser:User;
    editForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';

  constructor(
    private formBuilder: FormBuilder,
       
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private _http :UsersService,
    
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
   }

    ageRangeValidator(): ValidatorFn {
      console.log('in');
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        console.log(control.value);

       return { 'username': true };;
    };

  
}

get f() { return this.editForm.controls; }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      username: [this.currentUser.username,Validators.required , usernameAsyncValidator(this._http,this.authenticationService,this.currentUser)],
      name: [this.currentUser.name, Validators.required],
      email: [this.currentUser.email, Validators.required],
      phonenumber: [this.currentUser.number, Validators.required]

      
  });
  }

  isUserNameTaken(): boolean {
    console.log(this.editForm.errors)
    return this.editForm.hasError('UserExist');
    //this.editForm.errors
    
  }

  async onSubmit() {

    if (this.editForm.invalid) {
      return;
     }
    this.submitted = true;
    
    await this._http.updateProfile(this.f.username.value,this.f.name.value,this.f.email.value,this.f.phonenumber.value).then(data=>{
      var x = document.getElementById("snackbar");
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }).catch(err => {

      var x = document.getElementById("snackbar");
      x.className = "show";
      x.innerText = "An error has occured";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);

    });
    
}

}
