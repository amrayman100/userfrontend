import { Component, OnInit  , Output , EventEmitter} from '@angular/core';
import { AuthenticationService } from '../../_services/authentication.service';

import { Router } from '@angular/router';
import { User } from '../../_models/user';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  currentUser: User;
  @Output() messageToEmit = new EventEmitter<string>();
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
}

sendMessageToParent(message: string) {
  this.messageToEmit.emit(message)
}

logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
}
  ngOnInit() {
  }

  
}
