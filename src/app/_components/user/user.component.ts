import { Component, OnInit ,  ViewEncapsulation } from '@angular/core';
import { UsersService } from '../../users.service';
import {User} from '../../_models/user';
import { AuthenticationService } from '../../_services/authentication.service';
import { Group } from 'src/app/_models/group';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';



@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],

  

})
export class UserComponent implements OnInit {
   user : User;
   users:User[] = [];
   viewUser:User;
   viewGroup:Group;

   
   view: string = "profile";
  constructor(
    private _http :UsersService,
    private authenticationService: AuthenticationService,private breakpointObserver :BreakpointObserver
  ) { }
  
 

  ngOnInit() {
    console.log(this.view);
    this.user = this.authenticationService.currentUserValue;

    if(this.user.role=="user"){

      this._http.getUsers().toPromise().then(data =>{      
        for(var i = 0; i < data.length ; i++) {
      
          this.users.push({
            id:data[i][0],
            name:data[i][1],
            username:data[i][2],
            email:data[i][3],
            number:data[i][4]
          });
        }

        
      });
  

      this.breakpointObserver.observe([
        '(min-width: 768px)'
          ]).subscribe(result => {
            if (result.matches) {
              this.view = "profile";
     
            } else {
           
              
            }
          });
    }
    else{
     
      this._http.getUsers().toPromise().then(data =>{
        this.users = data;
        console.log(this.users);
      });

    }

  }

  
   getMessage(message: string) {
    console.log(message);
    this.view = message;
  
  }

  getMessage2({view:message,user: u}) {
    console.log("in");
    console.log(message);
    this.view = message;
    this.viewUser = u;

  
  }

  getMessage3({view:message,group: u}) {
    console.log("in");
    console.log(message);
    this.view = message;
    this.viewGroup = u;
 
  
  }

}

interface addModelArgs{
  view:string,
  user:User
}
