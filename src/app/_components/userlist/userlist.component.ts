import { Component, OnInit  , Output , EventEmitter, Input} from '@angular/core';
import { UsersService } from '../../users.service' 
import {User} from '../../_models/user';
import { AuthenticationService } from '../../_services/authentication.service';

@Component({
  selector: 'app-userlist2',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.scss']
})


export class UserlistComponent2 implements OnInit {
  @Output() messageToEmit = new EventEmitter<addModelArgs>();
  currentUser:User;
  @Input() users:User[];
  @Input() view:string;
  constructor(
    private _http :UsersService,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }
  ngOnInit() {
    console.log(this.users);
    
  }

  sendMessageToParent(message: string , u: User) {
    console.log("thisssss");
    console.log(u);
    this.messageToEmit.emit({view:message,user: u})
  }

}



interface addModelArgs{
  view:string,
  user:User
}