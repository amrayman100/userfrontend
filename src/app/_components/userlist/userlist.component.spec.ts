import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserlistComponent2 } from './userlist.component';

describe('UserlistComponent2', () => {
  let component: UserlistComponent2;
  let fixture: ComponentFixture<UserlistComponent2>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserlistComponent2 ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserlistComponent2);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
