import { Component, OnInit ,  Input , SimpleChanges , SimpleChange  ,Output , EventEmitter} from '@angular/core';
import {Group} from '../../_models/group';
import {User} from '../../_models/user';
import {GroupsService} from '../../_services/groups.service';
import { UsersService } from '../../users.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { BehaviorSubject, Observable } from 'rxjs';
@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {
  @Input() group : Group;
  @Output() messageToEmit = new EventEmitter<string>();
  currentUser:User;
  addUsers:User[] = [];
  users:User[] = [];
  constructor(private _http :GroupsService,private authenticationService: AuthenticationService , private _usershttp:UsersService) { }

  async ngOnInit() {
    
    this.currentUser = this.authenticationService.currentUserValue;
    await this._http.getGroupUsers3(this.group.id).then(res=>{
        this.users = res;
        console.log(this.users);
    });

    await this._http.getUnSubbed(this.users).then(res =>{
       this.addUsers = res;
    });

  }

  async ngOnChanges(changes: SimpleChanges) {
    const currentItem: SimpleChange = changes.item;
   
    
    await this._http.getGroupUsers3(changes.group.currentValue.id).then(res=>{
      this.users = res;
  });
    
  }

  async removeFromGroup(user:User){
   await this._usershttp.deleteUserFromGroup(user,this.group,).then(res=>{
     console.log("deleted")
    

   }).catch(err => {

    

  });

   await this._http.getGroupUsers3(this.group.id).then(res=>{
    this.users = res;
    console.log(this.users)
   });

   await this._http.getUnSubbed(this.users).then(res =>{
    this.addUsers = res;
   });
   
  }

  async addUserToGroup(user:User){
    await this._http.addUserToGroup(this.group,user).toPromise().then(res=>{
      console.log('added')
     
    }).catch(err => {
     
  
    });

    await this._http.getGroupUsers3(this.group.id).then(res=>{
      this.users = res;
      console.log(this.users);
   });

  await this._http.getUnSubbed(this.users).then(res =>{
    this.addUsers = res;
   });
  }

  deleteGroup(){
    console.log("Group Deleted");
    this._http.deleteGroup(this.group).subscribe(data =>{
       this.messageToEmit.emit("groupDeleted");
    });
  }

}
