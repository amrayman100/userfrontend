import { Component, OnInit ,  ViewEncapsulation } from '@angular/core';
import { UsersService } from '../../users.service';
import {User} from '../../_models/user';
import { AuthenticationService } from '../../_services/authentication.service';
import { Group } from 'src/app/_models/group';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  user : User;
  users:User[] = [];
  viewUser:User;
  viewGroup:Group;
  reload:boolean;
  //isSmallScreen = breakpointObserver.isMatched('(max-width: 599px)');
  
  view: string = "profile";
 constructor(
   private _http :UsersService,
   private authenticationService: AuthenticationService,private breakpointObserver :BreakpointObserver
 ) { }
 


 async ngOnInit() {
  this.breakpointObserver.observe([
    '(min-width: 768px)'
      ]).subscribe(result => {
        if (result.matches) {
          this.view = "profile";
          console.log("in");
        } else {
         
          
        }
      });
   
   this.reload = false;
   
   console.log(this.view);
   console.log(this.reload);
   this.user = this.authenticationService.currentUserValue;


   if(this.user.role=="user"){
    
    await this._http.getUsers().toPromise().then(data =>{      
       for(var i = 0; i < data.length ; i++) {
         this.users.push({
           id:data[i][0],
           name:data[i][1],
           username:data[i][2],
           email:data[i][3],
           number:data[i][4]
         });
       }
      
       
     });
     console.log(this.users);

       this.breakpointObserver.observe([
    '(min-width: 768px)'
      ]).subscribe(result => {
        if (result.matches) {
          this.view = "profile";
          console.log("in");
        } else {
         
          
        }
      });
   }
   else{
    
     await this._http.getUsers().toPromise().then(data =>{
       this.users = data;
       console.log(this.users);
     });

   }


 

 }

 
  getMessage(message: string) {
   console.log(message);
   this.view = message;
 
 }

 getMessage2({view:message,user: u}) {
   console.log("in");
   console.log(message);
   this.view = message;
   this.viewUser = u;

 
 }

 getMessage3({view:message,group: u}) {
   console.log("in");
   console.log(message);
   this.view = message;
   this.viewGroup = u;

 
 }

 loadUsers(message) {
  console.log("in");
  console.log(message);
  if(this.user.role=="user"){
    let dummyUsers: Object[] = [];
    this._http.getUsers().toPromise().then(data =>{      
      for(var i = 0; i < data.length ; i++) {
       
        this.users.push({
          id:data[i][0],
          name:data[i][1],
          username:data[i][2],
          email:data[i][3],
          number:data[i][4]
        });
      }
      
      
    });
    console.log(this.users);

   
  }
  else{
   
    this._http.getUsers().toPromise().then(data =>{
      this.users = data;
      
    });

    this.breakpointObserver.observe([
      '(min-width: 768px)'
        ]).subscribe(result => {
          if (result.matches) {
            this.view = "profile";
           
          } else {
            // if necessary:
            
          }
        });

  }


}
loadGroups($event){

  this.reload = !this.reload;
}

loadGroups2($event){
  this.view = "profile"
  this.reload = !this.reload;
}

}

interface addModelArgs{
 view:string,
 user:User
}
