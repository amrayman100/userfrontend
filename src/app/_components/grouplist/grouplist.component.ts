import { Component, OnInit  , Output , EventEmitter , SimpleChanges , SimpleChange, Input} from '@angular/core';
import {Group} from '../../_models/group';
import { GroupsService } from '../../_services/groups.service';
import { AuthenticationService } from '../../_services/authentication.service';
import {User} from '../../_models/user';


@Component({
  selector: 'app-grouplist',
  templateUrl: './grouplist.component.html',
  styleUrls: ['./grouplist.component.scss']
})
export class GrouplistComponent implements OnInit {
  @Output() messageToEmit = new EventEmitter<addModelArgs2>();
  currentUser:User;
  groups:Object= [];
  @Input() reload:boolean;
  constructor(
    private _http :GroupsService,
    private authenticationService: AuthenticationService
  ) {  this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {
 
    console.log(this.reload);
    this._http.getGroups().toPromise().then(data =>{
      this.groups = data;
 
    });
  }

  sendMessageToParent(message: string , u: Group) {
   
    this.messageToEmit.emit({view:message,group: u})
  }

  async ngOnChanges(changes: SimpleChanges) {
    const currentItem: SimpleChange = changes.item;
    
 
    console.log(changes.reload.currentValue)
    await this._http.getGroups().toPromise().then(data =>{
      this.groups = data;

    });


    
   
    
  }


}



interface addModelArgs2{
  view:string,
  group:Group
}
