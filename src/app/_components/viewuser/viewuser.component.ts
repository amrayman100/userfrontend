import { Component, OnInit , Input , Output , EventEmitter} from '@angular/core';
import {User} from '../../_models/user';
import { UsersService } from '../../users.service';
import { AuthenticationService } from '../../_services/authentication.service';
import { Group } from 'src/app/_models/group';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';

@Component({
  selector: 'app-viewuser',
  templateUrl: './viewuser.component.html',
  styleUrls: ['./viewuser.component.scss']
})
export class ViewuserComponent implements OnInit {
  @Input() user : User;
  currentUser: User;
  @Output() messageToEmit = new EventEmitter<string>();
  constructor(
    private _http :UsersService,
    private authenticationService: AuthenticationService,private breakpointObserver :BreakpointObserver
  ) { }

  ngOnInit() {
    console.log(this.user);
    this.currentUser = this.authenticationService.currentUserValue;
  }
  
  updateUser(u:User){
    console.log("updating user");
    this.user = u;

  }

  sendMessageToParent(message: string) {
    this.messageToEmit.emit(message)
  }
  

  async deleteUser(){
    await this._http.deleteUser(this.user).then(data=>{
        this.user = data;
    });
    this.user.del = true;
    this.messageToEmit.emit("users")
  }

  async undoDeleteUser(){
    await this._http.UndoDeleteUser(this.user).then(u=>{
        this.user = u;
    });
    this.user.del = false;
    this.messageToEmit.emit("users")
  }


}
