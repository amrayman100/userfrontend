import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import {User} from '../../_models/user';
import { AuthenticationService } from '../../_services/authentication.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  currentUser : User;
  @Output() messageToEmit = new EventEmitter<String>();
  constructor(
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  loadGroups(){
    console.log("FIRST EMIT");
    this.messageToEmit.emit("loadGroups");
  }

}
