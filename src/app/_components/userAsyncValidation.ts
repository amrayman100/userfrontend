import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl,FormBuilder, FormGroup, Validators , ValidatorFn , AbstractControl ,AsyncValidatorFn , ValidationErrors} from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../_services/authentication.service';
import { UsersService } from '../users.service' 
import {User} from '../_models/user';
import { Observable , timer } from "rxjs";
import { interval, fromEvent } from 'rxjs';
import { switchMap , map } from 'rxjs/operators';



export const usernameAsyncValidator = 
  (authService: UsersService,authService2: AuthenticationService, u:User, time: number = 500) => {
    console.log(u);
    //let currentUser: User = u;
    return (input: FormControl) => {
        console.log(input);
        /*if(input.value == u.username){
          return null;
        }*/
      return timer(time).pipe(
        switchMap(() => authService.getUser(input.value)),
        map(res => {
            console.log(res);
         
          return res==null || input.value == u.username  ? null : {UserExist: true}
        })
      );
    };
  };