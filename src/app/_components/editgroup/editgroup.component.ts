import { Component, OnInit, Input,SimpleChanges , SimpleChange ,Output , EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators , ValidatorFn , AbstractControl ,AsyncValidatorFn , ValidationErrors} from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../_services/authentication.service';
import { UsersService } from '../../users.service' 
import {GroupsService} from '../../_services/groups.service';
import {User} from '../../_models/user';
import {Group} from '../../_models/group';
import { Observable } from "rxjs";

import {usernameAsyncValidator} from '../userAsyncValidation';

@Component({
  selector: 'app-editgroup',
  templateUrl: './editgroup.component.html',
  styleUrls: ['./editgroup.component.scss']
})
export class EditgroupComponent implements OnInit {

  @Input() group : Group;
  editForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(private formBuilder: FormBuilder,
       
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private _http :GroupsService) { 

    }
get f() { return this.editForm.controls; }

  ngOnInit() {
   // console.log(this.currentUser);
    this.editForm = this.formBuilder.group({
      name: [this.group.name,Validators.required ],
      desc: [this.group.desc, Validators.required],
      
  });
  }

  isUserNameTaken(): boolean {
    console.log(this.editForm.errors)
    return this.editForm.hasError('UserExist');
    //this.editForm.errors
    
  }

  async onSubmit() {
  
    if (this.editForm.invalid) {
        return;
    }

    this.submitted = true;
    console.log(this.group.id);
    await this._http.updateGroup(this.f.name.value,this.f.desc.value,this.group).then(data=>{
      this.group = data;
      var x = document.getElementById("snackbar");
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  
    });
    


}
 ngOnChanges(changes: SimpleChanges) {
    const currentItem: SimpleChange = changes.item;

  this.editForm = this.formBuilder.group({
    name: [this.group.name,Validators.required ],
    desc: [this.group.desc, Validators.required],
    
  });

    
  }
}
