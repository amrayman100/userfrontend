import { Component, OnInit, Input,SimpleChanges , SimpleChange ,Output , EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators , ValidatorFn , AbstractControl ,AsyncValidatorFn , ValidationErrors} from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../_services/authentication.service';
import { UsersService } from '../../users.service' 
import {User} from '../../_models/user';
import { Observable } from "rxjs";
import {usernameAsyncValidator} from '../userAsyncValidation';



@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.scss']
})
export class EdituserComponent implements OnInit {
  @Input() currentUser:User;
  @Output() updatedUser = new EventEmitter<User>();
  editForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  constructor(
    private formBuilder: FormBuilder,
       
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private _http :UsersService,
    
  ) {
    
   }

    ageRangeValidator(): ValidatorFn {
      console.log('in');
    return (control: AbstractControl): { [key: string]: boolean } | null => {
        console.log(control.value);
    

      
       return { 'username': true };;
    };


}

get f() { return this.editForm.controls; }

  ngOnInit() {
    console.log(this.currentUser);
    this.editForm = this.formBuilder.group({
      username: [this.currentUser.username,Validators.required , usernameAsyncValidator(this._http,this.authenticationService,this.currentUser)],
      name: [this.currentUser.name, Validators.required],
      email: [this.currentUser.email, Validators.required],
      phonenumber: [this.currentUser.phoneNum, Validators.required]

      
  });
  }

  isUserNameTaken(): boolean {
    console.log(this.editForm.errors)
    return this.editForm.hasError('UserExist');
  
    
  }

  async onSubmit() {
    this.submitted = true;
    if (this.editForm.invalid) {
      return;
    }
    await this._http.updateUser(this.f.username.value,this.f.name.value,this.f.email.value,this.f.phonenumber.value,this.currentUser).then(data=>{
      this.currentUser = data;
      var x = document.getElementById("snackbar");
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
     
    }).catch(err => {

      var x = document.getElementById("snackbar");
      x.className = "show";
      x.innerText = "An error has occured";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);

    });
 
    this.updatedUser.emit(this.currentUser);
   


}
 ngOnChanges(changes: SimpleChanges) {
    const currentItem: SimpleChange = changes.item;

  this.editForm = this.formBuilder.group({
    username: [this.currentUser.username,Validators.required , usernameAsyncValidator(this._http,this.authenticationService,this.currentUser)],
    name: [this.currentUser.name, Validators.required],
    email: [this.currentUser.email, Validators.required],
    phonenumber: [this.currentUser.phoneNum, Validators.required]

    
});
    
  }
}
