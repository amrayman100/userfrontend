import { Component, OnInit, Input,SimpleChanges , SimpleChange ,Output , EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators , ValidatorFn , AbstractControl ,AsyncValidatorFn , ValidationErrors} from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../_services/authentication.service';
import { UsersService } from '../../users.service' 
import {GroupsService} from '../../_services/groups.service';
import {User} from '../../_models/user';
import {Group} from '../../_models/group';
import { Observable } from "rxjs";

import {usernameAsyncValidator} from '../userAsyncValidation';

@Component({
  selector: 'app-creategroup',
  templateUrl: './creategroup.component.html',
  styleUrls: ['./creategroup.component.scss']
})
export class CreategroupComponent implements OnInit {
  @Output() messageToEmit = new EventEmitter<String>();
  editForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(private formBuilder: FormBuilder,
       
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private _http :GroupsService) { }

    get f() { return this.editForm.controls; }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      name: [,Validators.required ],
      desc: [,Validators.required],
      
  });
  }

  async onSubmit() {
 
    
    if (this.editForm.invalid) {
        return;
    }

      this.submitted = true;
    
    await this._http.newGroup(this.f.name.value,this.f.desc.value).toPromise().then(data=>{
      
  
    }).catch(err => {

      var x = document.getElementById("snackbar");
      x.className = "show";
      x.innerText = "An error has occured";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);

    });

    this.messageToEmit.emit("loadGroups")
  }

  

}
